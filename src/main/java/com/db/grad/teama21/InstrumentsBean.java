package com.db.grad.teama21;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InstrumentsBean {
private ResultSet data;
	
	public void setData(ResultSet data) {
		this.data = data;
	}
	
	public boolean canProceed() {
		try {
			return !(data.isLast() || data.isAfterLast());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return false;
	}
	
	public String getRow() {
		try {
			data.next();
			int instrument_id = data.getInt(1);
			String instrument_name = data.getString(2);
			return new String(instrument_id + ", " + instrument_name + "<br>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
	
	public String getRowJSON() {
		try {
			data.next();
			int instrument_id = data.getInt(1);
			String instrument_name = data.getString(2);
			return new String("{\"instrument_id\":\"" + instrument_id + "\",\"instrument_name\":\"" + instrument_name + "\"}");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
}
