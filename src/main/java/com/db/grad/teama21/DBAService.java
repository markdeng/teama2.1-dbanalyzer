package com.db.grad.teama21;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;  

@Path("/hello")
public class DBAService extends Application{
	@GET 
	@Path("/hello")
    public Response getMsg()
    { 
        String output = "Jersey says hello world";
        return Response.status(200).entity(output).build(); 
    }  
}
