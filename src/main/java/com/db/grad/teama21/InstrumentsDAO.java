package com.db.grad.teama21;

import java.sql.ResultSet;

public class InstrumentsDAO {
	public static ResultSet getInstruments() {
	    DBConnector db = new DBConnector();
	    return db.getInstruments();
	}
	
}
