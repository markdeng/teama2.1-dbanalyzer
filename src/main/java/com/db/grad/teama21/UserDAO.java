package com.db.grad.teama21;

public class UserDAO {
	public static UserBean login(UserBean bean) {
		 String uid = bean.getUid();
		 String pwd = bean.getPwd();
	     DBConnector db = new DBConnector();
	     db.connect();
	     if (db.verifyUser(uid, pwd)) {
	    	 bean.setValid(true);
	     } else {
	    	 bean.setValid(false);
	     }
	     return bean;
		
	}
}
