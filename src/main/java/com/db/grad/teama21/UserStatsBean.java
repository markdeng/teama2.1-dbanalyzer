package com.db.grad.teama21;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserStatsBean {
	private ResultSet data;
	
	public void setData(ResultSet data) {
		this.data = data;
	}
	
	public boolean canProceed() {
		try {
			return !data.isLast();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return false;
	}
	
	public String getRow() {
		try {
			data.next();
			String uid = data.getString(1);
			String pwd = data.getString(2);
			
			String pwdDisplay = "";
			
			for (int i = 0; i < pwd.length(); i++) {
				pwdDisplay += "*";
			}
			return new String(uid + "," + pwdDisplay + "<br>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
	
	public String getRowJSON() {
		try {
			data.next();
			String uid = data.getString(1);
			String pwd = data.getString(2);
			
			String pwdDisplay = "";
			
			for (int i = 0; i < pwd.length(); i++) {
				pwdDisplay += "*";
			}
			return new String("{\"uid\":\"" + uid + "\", \"pwd\":\"" + pwdDisplay + "\"}");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
		return null;
	}
}
