<%@page contentType="text/html" pageEncoding="UTF-8"%>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
		<script>
		let loginOptions = {
				method:'post',
				mode:'cors',
				credentials:'include'
		};
		
		let dealsOptions = {
				method:'post',
				mode:'cors',
				credentials:'include'
		};
		
		fetch('api.jsp?function=login&uid=selvyn&pwd=gradprog2016', loginOptions).then(function(response){
				return response.json();
			}).then(function(loginJson){
				console.log(loginJson)
			}).then(function(){
				fetch('api.jsp?function=deals&filter=ID;GreaterThan;21000', dealsOptions).then(function(response){
					return response.json();
				}).then(function(dealJson){
					console.log(dealJson);
					document.write(JSON.stringify(dealJson));
				})
			})
		</script>
    </body>
</html>